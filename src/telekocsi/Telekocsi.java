package telekocsi;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Telekocsi {

    class Auto {

        private String indulas;
        private String cel;
        private String rendszam;
        private String telefonszam;
        private int ferohely;
        private final List<Igeny> igenyek = new ArrayList<>();
        private int szabadFerohely;

        public Auto() {
        }

        public Auto(String indulas, String cel, String rendszam, String telefonszam, int ferohely) {
            this.indulas = indulas;
            this.cel = cel;
            this.rendszam = rendszam;
            this.telefonszam = telefonszam;
            this.ferohely = ferohely;
            this.szabadFerohely = this.ferohely;
        }

        public String getIndulas() {
            return indulas;
        }

        public void setIndulas(String indulas) {
            this.indulas = indulas;
        }

        public String getCel() {
            return cel;
        }

        public void setCel(String cel) {
            this.cel = cel;
        }

        public String getRendszam() {
            return rendszam;
        }

        public void setRendszam(String rendszam) {
            this.rendszam = rendszam;
        }

        public String getTelefonszam() {
            return telefonszam;
        }

        public void setTelefonszam(String telefonszam) {
            this.telefonszam = telefonszam;
        }

        public int getFerohely() {
            return ferohely;
        }

        public void setFerohely(int ferohely) {
            this.ferohely = ferohely;
        }

        public int getSzabadFerohely() {
            return szabadFerohely;
        }

        public void addIgeny(Igeny igeny) {
            if (szabadFerohely >= igeny.szemelyek) {
                igenyek.add(igeny);
                szabadFerohely -= igeny.szemelyek;
            }
        }
    }

    class Igeny {

        private String azonosito;
        private String indulas;
        private String cel;
        private int szemelyek;
        private Auto auto;

        public Igeny() {
        }

        public Igeny(String azonosito, String indulas, String cel, int szemelyek) {
            this.azonosito = azonosito;
            this.indulas = indulas;
            this.cel = cel;
            this.szemelyek = szemelyek;
        }

        public String getAzonosito() {
            return azonosito;
        }

        public void setAzonosito(String azonosito) {
            this.azonosito = azonosito;
        }

        public String getIndulas() {
            return indulas;
        }

        public void setIndulas(String indulas) {
            this.indulas = indulas;
        }

        public String getCel() {
            return cel;
        }

        public void setCel(String cel) {
            this.cel = cel;
        }

        public int getSzemelyek() {
            return szemelyek;
        }

        public void setSzemelyek(int szemelyek) {
            this.szemelyek = szemelyek;
        }

        public Auto getAuto() {
            return auto;
        }

        public void setAuto(Auto auto) {
            this.auto = auto;
        }
    }

    private List<Auto> autok;
    private List<Igeny> igenyek;

    private void autokBeolvas() throws FileNotFoundException, UnsupportedEncodingException, IOException {
        autok = new ArrayList<>();
        File autokFile = new File("./files/autok.csv");
        BufferedReader br = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(autokFile), "UTF-8"));
        String sor;
        br.readLine();
        while ((sor = br.readLine()) != null) {
            String[] adatok = sor.split(";");
            Auto auto = new Auto(
                    adatok[0],
                    adatok[1],
                    adatok[2],
                    adatok[3],
                    Integer.parseInt(adatok[4]));
            autok.add(auto);
        }
        br.close();
    }

    private void igenyekBeolvas() throws FileNotFoundException, UnsupportedEncodingException, IOException {
        igenyek = new ArrayList<>();
        File igenyekFile = new File("./files/igenyek.csv");
        BufferedReader br = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(igenyekFile), "UTF-8"));
        String sor;
        br.readLine();
        while ((sor = br.readLine()) != null) {
            String[] adatok = sor.split(";");
            Igeny igeny = new Igeny(
                    adatok[0],
                    adatok[1],
                    adatok[2],
                    Integer.parseInt(adatok[3]));
            igenyek.add(igeny);
        }
        br.close();
    }

    private void run() throws UnsupportedEncodingException, IOException {
        autokBeolvas();
        igenyekBeolvas();
        feladat2();
        feladat3();
        feladat4();
        feladat5();
        feladat6();
    }

    private void feladat2() {
        System.out.println("2. feladat");
        System.out.println(autok.size() + " autos hirdet fuvart");
        System.out.println();
    }

    private void feladat3() {
        String indulas = "Budapest";
        String cel = "Miskolc";
        int sum = 0;
        for (Auto auto : autok) {
            if (auto.getIndulas().equals(indulas) && auto.getCel().equals(cel)) {
                sum += auto.getFerohely();
            }
        }
        System.out.println("3. feladat");
        System.out.println("Összesen " + sum + " férőhelyet hirdettek az autósok Budapestről Miskolcra");
        System.out.println();
    }

    private void feladat4() {
        Map<String, Integer> utvonalak = new HashMap<>();
        for (Auto auto : autok) {
            String utvonal = auto.getIndulas() + "-" + auto.getCel();
            if (utvonalak.get(utvonal) == null) {
                utvonalak.put(utvonal, auto.getFerohely());
            } else {
                utvonalak.put(utvonal, utvonalak.get(utvonal) + auto.getFerohely());
            }
        }

        int max = 0;
        String maxUtvonal = "";
        for (Map.Entry<String, Integer> entry : utvonalak.entrySet()) {
            if (entry.getValue() > max) {
                max = entry.getValue();
                maxUtvonal = entry.getKey();
            }
        }

        System.out.println("4. feladat");
        System.out.println("A legtöbb férőhelyet ("
                + max + "-et) a "
                + maxUtvonal + " útvonalon ajánlották fel a hirdetők");
        System.out.println();
    }

    private void feladat5() {
        System.out.println("5. feladat");
        for (Igeny igeny : igenyek) {
            int i = 0;
            boolean talalat = false;
            while (i < autok.size() && !talalat) {
                Auto auto = autok.get(i);
                talalat = igeny.getIndulas().equals(auto.getIndulas())
                        && igeny.getCel().equals(auto.getCel())
                        && igeny.getSzemelyek() <= auto.getSzabadFerohely();
                if (!talalat) {
                    i++;
                }
            }

            if (talalat) {
                Auto auto = autok.get(i);
                auto.addIgeny(igeny);
                igeny.setAuto(auto);
                System.out.println(igeny.getAzonosito() + " => " + auto.getRendszam());
            }
        }
        System.out.println();
    }

    private void feladat6() throws IOException {
        File file = new File("./files/utasuzenetek.txt");
        BufferedWriter bw = new BufferedWriter(
                new FileWriter(file));
        for (Igeny igeny : igenyek) {
            String uzenet;
            if (igeny.getAuto() == null) {
                uzenet = igeny.getAzonosito()
                        + ": Sajnos nem sikerült autót találni";
            } else {
                Auto auto = igeny.getAuto();
                uzenet = igeny.getAzonosito()
                        + ": Rendszám: " + auto.getRendszam()
                        + ", Telefonszám: " + auto.getTelefonszam();
            }
            bw.append(uzenet);
            bw.append(System.lineSeparator());
        }
        bw.close();
    }

    public static void main(String[] args) throws IOException {
        new Telekocsi().run();
    }
}
